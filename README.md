Project Manager Module
=============================

With the `fscm-project-manager` module it is possible to handle multiple
different projects without any project specific code in the `.fluent` file. You
register the paths to your projects at the project manager and it takes care
that only the code that is needed when you start fluent is executed. Below the
installation and basic usage of this module is described, if you are looking for
the API documentation [click here][api-doc] (located in the `doc` folder).

If you'd like to contribute, please read the 
[contribution guide][contributing] first.

Installation
------------
Before you can install the project manager module you have to properly setup the
module manager. Follow the 
[Fluent Scheme Module Manager][fscm-module-manager-repo]
guide to set it up.

Now you need to clone the module repository to your computer. Choose a proper
location where you want to clone it, preferably the same directory where the
module manager repository is located (e.g.: `/home/<user name>/jkuisw`). Go into
that directory and clone the repository, as follows:

```shell
cd "/home/<user name>/jkuisw"
git clone --recurse-submodules git@gitlab.com:jkuisw/fscm-project-manager.git
```

After cloning the repository ensure that the `fscm-project-manager` module is 
registered to the module manager, as described in the 
[module manager setup][fscm-module-manager-register-modules] (`add-module-paths`
procedure).

Usage
-----
To use the project manager module you have to import it with a proper import
name. This should usually be done in the `.fluent` file, as well as the
configuration of it. The following diagram shows the structure of the `.fluent`
file when the project manager is used.

![.fluent file structure][dot-fluent-file-structure]

As you can see, in the `.fluent` file you register your projects (paths to the
projects) with the `add-project-exec-init` command. When you start fluent the
`.fluent` file is executed and therefore the `add-project-exec-init` command
is invoked. The `add-project-exec-init` procedure checks then if fluent was
started in the given project path (passed as parameter to the procedure) or any
of it's sub-directories. If so, it checks the directory where fluent was started
for existing initialization file(s), which is by default a file called
`fluent-init.scm` or a custom file name when passed to the
`add-project-exec-init` procedure (could be a list of files to load, see the
[API documentation][api-doc]). When any initialization file was found in that
directory, it is executed. The following flow chart shows roughly how the
`add-project-exec-init` command works.

![add-project-exec-init command flow chart][flow-chart_add-project-exec-init]

So consider you configured multiple project paths, than it depends on the
directory where you start fluent, which files are executed (loaded). The
following code snippet (as mentioned above, this should be added to the
`.fluent` file) shows an example configuration.

```scheme
; Import the project manager module.
(import "pman" "fscm-project-manager")

; Register a project path to the project manager.
(pman/add-project-exec-init "/path/to/the/project1")

; Register another project, but execute the "custom-init-file-name.scm" script
; instead of the default "fluent-init.scm" script.
(pman/add-project-exec-init "/path/to/the/project2" "custom-init-file-name.scm")

; Register another project, but execute all scripts in the passed list instead
; of the default "fluent-init.scm" script.
(pman/add-project-exec-init "/path/to/the/project3" (list
  "custom-init-file-name-1.scm"
  "custom-init-file-name-2.scm"
  "custom-init-file-name-3.scm"
))
```

In the example above three projects are registered. As mentioned earlier only one
will load the initialization file(s), depending on in which directory fluent is
started (or no initialization file(s) will be loaded if fluent is not started in 
any of the project paths).  

Furthermore the project manager provides a command called `reload-projects`.
This procedure is useful during development, because when you execute the
command it reloads the initialization files of the current project.  
The full documentation for all procedures of the project manager module can be
found in the [API documentation][api-doc].

<!-- links reference -->
[api-doc]: doc/README.md
[contributing]: CONTRIBUTING.md

[fscm-module-manager-repo]: https://gitlab.com/jkuisw/fscm-module-manager
[fscm-module-manager-register-modules]: https://gitlab.com/jkuisw/fscm-module-manager#register-modules4


<!-- images reference -->
[dot-fluent-file-structure]: /resources/dot-fluent-file-structure.png
[flow-chart_add-project-exec-init]: /resources/flow-chart_add-project-exec-init.png
