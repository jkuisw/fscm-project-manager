# Module: fscm-project-manager
This is the overview of the module documentation. In this file you can find a list with all source files of the module, a list of all exported procedures of the module and a list of all module private procedures. The name of each list entry links to the specific documentation of the source file, exported procedure or private procedure.  

## Source Files
A list of all source files of this module.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: project-manager](/doc/project-manager.md) | Implements the core functionality of the project manager. |

## Exported Procedures
A list of all exported procedures of this module. The procedure names are the internal procedure names. Normally this is the same as the exported name, prepended with the import name, but thy can differ. Look into the detailed procedure documentation to get the export name of the procedure. Exported procedures are imported into a source file with the `import` procedure (see the fscm-module-manager module for further details), which takes the import name as the first parameter. Therefore every exported procedure of a module will be imported according to the following syntax:  
`<import name>/<export name of the procedure>`.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: add-project-exec-init](/doc/project-manager.md#add-project-exec-init) | Adds the given project path as project folder. Than checks if the currentdir... |
| [:page_facing_up: reload-projects](/doc/project-manager.md#reload-projects) | Reloads all registered projects. |

## Private Procedures
A list of all private procedures of this module. This procedures are not accessible from outside the module.  

| Name | Description |
|:---- |:----------- |
| [:page_facing_up: get-project-subpath](/doc/project-manager.md#get-project-subpath) | Checks if the given path is in the project directory and if so returns thepa... |
| [:page_facing_up: add-project-path](/doc/project-manager.md#add-project-path) | Adds the project to the projects list. |
| [:page_facing_up: load-project](/doc/project-manager.md#load-project) | Loads the given project. The procedure will load all init files that areexis... |

