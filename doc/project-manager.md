:arrow_left: [Go back to the overview.](/doc/README.md)
  
  
  
  
# File: project-manager.scm
Implements the core functionality of the project manager.

:arrow_right: [Go to source code of this file.](/src/project-manager.scm)

## Exported Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: add-project-exec-init](#add-project-exec-init) | Adds the given project path as project folder. Than checks if the currentdir... |
| [:page_facing_up: reload-projects](#reload-projects) | Reloads all registered projects. |

## Private Procedures
| Name | Description |
|:---- |:----------- |
| [:page_facing_up: get-project-subpath](#get-project-subpath) | Checks if the given path is in the project directory and if so returns thepa... |
| [:page_facing_up: add-project-path](#add-project-path) | Adds the project to the projects list. |
| [:page_facing_up: load-project](#load-project) | Loads the given project. The procedure will load all init files that areexis... |

## Procedure Documentation

### get-project-subpath

#### Syntax
```scheme
(get-project-subpath project-dir path)
```

:arrow_right: [Go to source code of this procedure.](/src/project-manager.scm#L23)

#### Description
Checks if the given path is in the project directory and if so returns the
path part relative to the project directory.

#### Parameters
##### `project-dir`  
The absolute project directory.

##### `path`  
The absolute path where fluent was started.


#### Returns
False if the path is not in the project directory, otherwise the part
of the path relative to the project directory.

-------------------------------------------------
### add-project-path

#### Syntax
```scheme
(add-project-path project-path init-file-names exec-dir)
```

:arrow_right: [Go to source code of this procedure.](/src/project-manager.scm#L81)

#### Description
Adds the project to the projects list.

#### Parameters
##### `project-path`  
The project path.

##### `init-file-names`  
A list of init file names.

##### `exec-dir`  
The directory where fluent was started. Files in that directory
matching with the init file names will be loaded/executed. The path
must be relative from the project path.


#### Returns
The created or updated project entry.

-------------------------------------------------
### load-project

#### Syntax
```scheme
(load-project project)
```

:arrow_right: [Go to source code of this procedure.](/src/project-manager.scm#L161)

#### Description
Loads the given project. The procedure will load all init files that are
existing in the exec directory of the configured project.

#### Parameters
##### `project`  
The project (entry from the projects list) to load.


#### Returns
True if successful, false otherwise.

-------------------------------------------------
### add-project-exec-init

#### Syntax
```scheme
(add-project-exec-init project-path [init-file-names] [max-depth])
```

:arrow_right: [Go to source code of this procedure.](/src/project-manager.scm#L224)

#### Export Name
`<import-name>/add-project-exec-init`

#### Description
Adds the given project path as project folder. Than checks if the current
directory (which should be the directory in which fluent was started) is the
project folder or a subdirectory (according to the optional max-depth
parameter) of the project folder. If this is the case, than it searches for a
`"fluent-init.scm"` file (or according to the optinal `init-file-names`
parameter) in that directory and executes (loads) it.

#### Parameters
##### `project-path`  
The project directory to add. Fluent should be started in
this or any subdirectory so that the init file will be executed. Otherwise
the init file will not be executed.

##### `init-file-names`  
_Attributes: optional, default: `"fluent-init.scm"`_  
If a string, than
the name of the init file, if a list than a list of init file names.
All init files in the list (or just the one if passed a string) will be
loaded/executed when they exist in the directory where fluent was started
(= current directory). But only if fluent was started in the project path
or a subdirectory of the project path.

##### `max-depth`  
_Attributes: optional, default: `20`_  
The maximum number of subdirectories
under the project path init files will be started. e.g.:
+ `max-depth` = `2`
+ `project-path` = `"/path/to/project"`
+ `current-path` (where fluent was started) = `"/path/to/project/dir1"`  
  => init file in the directory `"/path/to/project/dir1"` will be executed
+ `current-path` (where fluent was started) = `"/path/to/project/dir1/dir2"`  
  => init file in the directory `"/path/to/project/dir1/dir2"` will
  **NOT** be executed


#### Returns
True if init file has been loaded, false otherwise.

-------------------------------------------------
### reload-projects

#### Syntax
```scheme
(reload-projects)
```

:arrow_right: [Go to source code of this procedure.](/src/project-manager.scm#L327)

#### Export Name
`<import-name>/reload-projects`

#### Description
Reloads all registered projects.

#### Returns
True if every project was loaded successfully. If any project failed
to load than false.

