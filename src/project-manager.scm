;*******************************************************************************
; Implements the core functionality of the project manager.

;*******************************************************************************
; Dependencies
(import "utils" "fscm-utils" "1.*.*")
(import "fs" "fscm-file-system-ops" "1.*.*")

;*******************************************************************************
; Module global variables.
(define projects-list '())

;*******************************************************************************
; Checks if the given path is in the project directory and if so returns the
; path part relative to the project directory.
;
; Parameters:
;   project-dir - The absolute project directory.
;   path - The absolute path where fluent was started.
;
; Returns: False if the path is not in the project directory, otherwise the part
;   of the path relative to the project directory.
(define (get-project-subpath project-dir path) (let (
    (proj-dir-comps (utils/string-split project-dir #\/))
    (path-comps (utils/string-split path #\/))
    (proj-comp "")
    (path-comp "")
  )

  (let next-comp ((proj-dirs proj-dir-comps) (path-dirs path-comps))
    (cond
      ; When reached end of project directory components list, than the path is
      ; in the project directory and the rest of the path components list is the
      ; subpath to return.
      ((null? proj-dirs) (utils/string-join path-dirs "/"))
      ; If not reached the end of the project directory components list, but
      ; reached the end of the path components list, than the path is not in the
      ; project directory. Therefore return false.
      ((null? path-dirs) #f)
      ; The proj-dirs and path-dirs list containing both at least one element,
      ; therefore compare the current components of both lists (which are the
      ; first entries of the lists).
      (else (begin
        (set! proj-comp (car proj-dirs))
        (set! path-comp (car path-dirs))

        (cond
          ; If current project component is an empty string, take the next one.
          ((= (string-length proj-comp) 0)
            (next-comp (cdr proj-dirs) path-dirs)
          )
          ; If current path component is an empty string, take the next one.
          ((= (string-length path-comp) 0)
            (next-comp proj-dirs (cdr path-dirs))
          )
          ; Compare the components, they must be equal when the path is in the
          ; project directory.
          ((string=? proj-comp path-comp)
            ; Components are equal, therefore go to the next components..
            (next-comp (cdr proj-dirs) (cdr path-dirs))
          )
          ; Path is not in the project directory, therefore return false.
          (else #f)
        )
      ))
    )
  )
))

;*******************************************************************************
; Adds the project to the projects list.
;
; Parameters:
;   project-path - The project path.
;   init-file-names - A list of init file names.
;   exec-dir - The directory where fluent was started. Files in that directory
;     matching with the init file names will be loaded/executed. The path
;     must be relative from the project path.
;
; Returns: The created or updated project entry.
(define (add-project-path project-path init-file-names exec-dir)
  (letrec (
      ; Procedure updates the init files in the passed project settings.
      ;   prj - The current project settings.
      ;   files - The init file names to add.
      (add-init-files (lambda (prj files) (let (
          (curr-files (utils/map-get-value prj 'init-file-names))
        )
        ; Loop through the init file names and add only new ones.
        (for-each (lambda (file)
          ; Only add if not already in list.
          (let next-entry ((remaining-entries curr-files))
            (cond
              ; If reached end of list, than file was not added before.
              ((null? remaining-entries)
                (if (null? curr-files)
                  (set! curr-files (list file))
                  (utils/append-to-list curr-files file)
                )
              )
              ; Check if file is equal to current file in list, if so break out
              ; of loop.
              ((string=? (car remaining-entries) file) #t)
              ; File is not equal to current file in list, therefore go to the
              ; next entry.
              (else (next-entry (cdr remaining-entries)))
            )
          )
        ) files)
        ; Update the files list in the project settings.
        (utils/map-set-value prj 'init-file-names curr-files)
      )))
      (new-project-entry (list
        ; The project path.
        (list 'project-path project-path)
        ; The directory where fluent was started. Files in that directory
        ; matching with the init file names will be loaded/executed. The path
        ; must be relative from the project path.
        (list 'exec-dir exec-dir)
        ; List of init file names.
        (list 'init-file-names init-file-names)
      ))
    )
    ; Loop through projects list to check if the project-path is already added,
    ; if not, add the project-path.
    (let next-project ((remaining-projects projects-list))
      (cond
        ; When reached end of list, than project was not added.
        ((null? remaining-projects) (begin
          ; Add the new project to the projects list.
          (if (null? projects-list)
            (set! projects-list (list new-project-entry))
            (utils/append-to-list projects-list new-project-entry)
          )
          new-project-entry ; Return the created entry.
        ))
        ; Check current entry.
        ((string=?
            (utils/map-get-value (car remaining-projects) 'project-path)
            project-path
          )
          ; Found existing entry, update init file names.
          (add-init-files (car remaining-projects) init-file-names)
          (car remaining-projects) ; Return the updated entry
        )
        ; Current entry not equal to project-path, go to the next entry.
        (else (next-project (cdr remaining-projects)))
      )
    )
  )
)

;*******************************************************************************
; Loads the given project. The procedure will load all init files that are
; existing in the exec directory of the configured project.
;
; Parameters:
;   project - The project (entry from the projects list) to load.
;
; Returns: True if successful, false otherwise.
(define (load-project project)
  (if (null? project) (begin
    #f ; Return false.
  ) (begin ; else
    ; Loop through the init file names of the project and load each existing
    ; one.
    (let next-init-file (
        (remaining-files (utils/map-get-value project 'init-file-names))
      )
      (if (null? remaining-files) (begin
        ; Reached end of list, all existing files loaded therefore return
        ; true.
        #t
      ) (let ( ; else
          (project-path (utils/map-get-value project 'project-path))
          (exec-dir (utils/map-get-value project 'exec-dir))
          (file-name (car remaining-files))
          (file-path "")
        )

        ; Load current init file if existing.
        (set! file-path (format #f "~a/~a/~a" project-path exec-dir file-name))
        (if (file-exists? file-path) (begin
          ; Load the init file.
          (load file-path user-initial-environment)
        ))

        ; Go to the next init file in the list.
        (next-init-file (cdr remaining-files))
      ))
    )
  ))
)

;*******************************************************************************
; Adds the given project path as project folder. Than checks if the current
; directory (which should be the directory in which fluent was started) is the
; project folder or a subdirectory (according to the optional max-depth
; parameter) of the project folder. If this is the case, than it searches for a
; `"fluent-init.scm"` file (or according to the optinal `init-file-names`
; parameter) in that directory and executes (loads) it.
;
; Parameters:
;   project-path - The project directory to add. Fluent should be started in
;     this or any subdirectory so that the init file will be executed. Otherwise
;     the init file will not be executed.
;   init-file-names - [optional, default: "fluent-init.scm"] If a string, than
;     the name of the init file, if a list than a list of init file names.
;     All init files in the list (or just the one if passed a string) will be
;     loaded/executed when they exist in the directory where fluent was started
;     (= current directory). But only if fluent was started in the project path
;     or a subdirectory of the project path.
;   max-depth - [optional, default: 20] The maximum number of subdirectories
;     under the project path init files will be started. e.g.:
;     + `max-depth` = `2`
;     + `project-path` = `"/path/to/project"`
;     + `current-path` (where fluent was started) = `"/path/to/project/dir1"`  
;       => init file in the directory `"/path/to/project/dir1"` will be executed
;     + `current-path` (where fluent was started) = `"/path/to/project/dir1/dir2"`  
;       => init file in the directory `"/path/to/project/dir1/dir2"` will
;       **NOT** be executed
;
; Returns: True if init file has been loaded, false otherwise.
(define (add-project-exec-init project-path . args) (let (
    (init-file-names (list "fluent-init.scm"))
    (init-file-path "")
    (max-depth 20)
    (first-par-is 'invalid)
    (arg 0)
    (argc (length args))
    (current-dir "")
    (project-dir "")
    (subpath "")
    (project '())
  )

  ; Parse optional arguments.
  (if (>= argc 1) (begin
    (set! arg (list-ref args 0))
    (cond
      ; Check if init file name parameter was passed.
      ((or (string? arg) (symbol? arg) (pair? arg)) (begin
        (if (or (string? arg) (symbol? arg))
          (set! init-file-names (list (utils/to-string arg)))
          (set! init-file-names (utils/object-list->string-list arg))
        )
        (set! first-par-is 'init-file-names)
      ))
      ; Check if max depth parameter was passed.
      ((number? arg) (begin
        (set! max-depth arg)
        (set! first-par-is 'max-depth)
      ))
      ; If nothing matches, than the passed parameter is invalid.
      (else (error "Invalid parameter!" arg))
    )
  ))

  ; Parse optional arguments.
  (if (> argc 1) (begin
    (set! arg (list-ref args 1))
    (cond
      ; Check if init file name parameter was passed.
      ((and
          (eqv? first-par-is 'max-depth)
          (or (string? arg) (symbol? arg) (pair? arg))
        )
        (if (or (string? arg) (symbol? arg))
          (set! init-file-names (list (utils/to-string arg)))
          (set! init-file-names (utils/object-list->string-list arg))
        )
      )
      ; Check if max depth parameter was passed.
      ((and (eqv? first-par-is 'init-file-names) (number? arg))
        (set! max-depth arg)
      )
      ; If nothing matches, than the passed parameter is invalid.
      (else (error "Invalid parameter!" arg))
    )
  ))

  ; Retieve the current directory.
  (set! current-dir (fs/get-current-directory))
  (if (eqv? current-dir #f)
    (error "Failed to retrieve the current directory!" current-dir)
  )

  ; Resolve the path.
  (set! project-dir (fs/resolve-path project-path))
  (if (eqv? project-dir #f)
    (error "Failed to resolve the project path!" project-path)
  )

  ; Retrieve the relative path from the project directory of the current
  ; directory
  (set! subpath (get-project-subpath project-dir current-dir))

  ; Only continue if fluent start directory (= current directory) is in the
  ; project directory.
  (if (not (eqv? subpath #f)) (begin
    ; Only continue if number of subpath directories is lesser or equal than the
    ; maximum depth parameter.
    (if (<= (length (utils/string-split subpath #\/)) max-depth) (begin
      ; Register the project.
      (set! project
        (add-project-path project-dir init-file-names subpath)
      )

      ; Load the project and return the result of the procedure, which is true
      ; when successfully loaded the project and false otherwise.
      (load-project project)
    ) (begin ; else
      #f ; Init file was not loaded/executed, because number of subpath
      ; directories is bigger than maximum depth parameter.
    ))
  ) (begin ; else
    #f ; Init file was not loaded/executed, because fluent start directory
    ; (= current directory) is not in the project path.
  ))
))

;*******************************************************************************
; Reloads all registered projects.
;
; Returns: True if every project was loaded successfully. If any project failed
; to load than false.
(define (reload-projects) (let ((success #t))
  ; Loop through all projects and load each.
  (for-each (lambda (project)
    (if (not (load-project project)) (success #f))
  ) projects-list)

  ; Return true if every project was loaded successfully. If any project failed
  ; to load return false.
  success
))

;*******************************************************************************
; Export public procedures.
(export (list
  (list 'add-project-exec-init add-project-exec-init)
  (list 'reload-projects reload-projects)
))

;*******************************************************************************
; TODO
;
; Parameters:
;   par - description
;
; Returns: TODO
